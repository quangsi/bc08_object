function layThongTin() {
  //   lấy thong tin từ form
  var _ma = document.getElementById("txtMaSV").value;
  var _ten = document.getElementById("txtTenSV").value;
  var _loai = document.getElementById("loaiSV").value;
  var _toan = document.getElementById("txtDiemToan").value * 1;
  var _van = document.getElementById("txtDiemVan").value * 1;
  return {
    ma: _ma,
    ten: _ten,
    loai: _loai,
    toan: _toan,
    van: _van,
    tinhDTB: function () {
      var dtb = (this.toan + this.van) / 2;
      return dtb;
    },
    xepLoai: function () {
      var dtb = this.tinhDTB();
      if (dtb >= 5) {
        return "Đạt";
      } else {
        return "Rớt";
      }
    },
  };
}

function showThongTin() {
  var sv = layThongTin();
  // show thông tin
  document.getElementById("spanMaSV").innerText = sv.ma;
  document.getElementById("spanTenSV").innerText = sv.ten;
  document.getElementById("spanLoaiSV").innerText = sv.loai;
  document.getElementById("spanDTB").innerText = sv.tinhDTB();
  document.getElementById("spanXepLoai").innerText = sv.xepLoai();
}
//
