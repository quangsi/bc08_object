// hợp lệ => true
function kiemTraRong(idErr, value) {
  if (value.trim().length == 0) {
    // lỗi
    document.getElementById(idErr).innerText = "Nội dung này không được để trống";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraDoDai(min, max, idErr, value) {
  // trim ~ remove khoảng trắng
  var length = value.trim().length;
  if (length < min || length > max) {
    document.getElementById(idErr).innerText = `Nội dung phải từ ${min} đến ${max} kí tự`;
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
function kiemTraEmail(idErr, value) {
  const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (re.test(value)) {
    document.getElementById(idErr).innerText = "";
    return true;
  } else {
    document.getElementById(idErr).innerText = "email không hợp lệ";

    return false;
  }
}
