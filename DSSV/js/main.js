var dssv = []; // array chứa object
var jsonData = localStorage.getItem("DSSV");
if (jsonData != null) {
  var list = JSON.parse(jsonData);
  dssv = list.map(function (item) {
    return new SinhVien(item.ma, item.ten, item.email, item.matKhau, item.toan, item.ly, item.hoa);
  });

  renderDSSV(dssv);
  // map js => convert
}

function themSv() {
  var sv = layThongTinTuForm();
  // start validate

  // tenSv
  // && , &, true => 1, false => 0

  var isValid =
    kiemTraRong("spanTenSV", sv.ten) &
    kiemTraDoDai(2, 30, "spanTenSV", sv.ten) &
    kiemTraDoDai(2, 30, "spanMatKhau", sv.matKhau) &
    kiemTraEmail("spanEmailSV", sv.email);
  // end validate

  // regex

  if (!isValid) return;
  // nếu ko hợp lệ thì kết thúc tại dòng 26
  dssv.push(sv);
  // lưu dữ liệu vào localStorage
  var jsonData = JSON.stringify(dssv);
  localStorage.setItem("DSSV", jsonData);

  renderDSSV(dssv);
  //   tbodySinhVien=> innerHTML 1 string gồm các thẻ tr
  //   tại sao từ 2 còn 1
  //    lấy dữ liệu lên khi load trang=> localStorage.getItem, JSON.parse
}
function xoaSv(id) {
  // chỉ dùng vs number, string thì lỗi
  // vị trí
  // splice

  var index = timViTri(id, dssv);
  dssv.splice(index, 1);

  console.log("😀 - xoaSv - dssv", dssv);
  renderDSSV(dssv);
  // var jsonData = JSON.stringify(dssv);
  // localStorage.setItem("DSSV", jsonData);
}
function suaSv(id) {
  var index = timViTri(id, dssv);
  var sv = dssv[index];
  showThongTinLenForm(sv);
  document.getElementById("txtMaSV").disabled = true;
}
function capNhatSv() {
  // ko cho user update id

  var sv = layThongTinTuForm();
  var index = timViTri(sv.ma, dssv);
  dssv[index] = sv;
  renderDSSV(dssv);
}
// firefox browser, localStorage, Json stringtify ,Json parse,
