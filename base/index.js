// data type: pass by value , pass by reference

// pass by value: string, number,boolean
// pass by reference: array, object

var svAlice = {
  // key:value,
  username: "Alice",
  //   propterty
  email: "alice@gmail.com",
  address: "112 Cao Thang",
  sayHello: function () {
    //method
    console.log("Hello", this.username);
  },
};
// key: property, method

svAlice.username = "AliceTran";
console.log("🚀 - svAlice", svAlice);

// class

// class Cat{
//   this.name
// }

function Cat(_name, _age) {
  this.ten = _name;
  this.tuoi = _age;
}

var cat1 = new Cat("meo meo", 2);
var cat2 = new Cat("me me", 1);
console.log("😀 - cat1", cat1);
console.log("😀 - cat2", cat2);
